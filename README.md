# Fluidcord
Fluidcord

![preview](https://i.imgur.com/LrhiToU.png)

## How To Install Fluidcord?

Its Simple!

For **Powercord** Users:

1. Open CMD & Type:

```
cd powercord/src/Powercord/themes && git clone https://github.com/leeprky/Fluidcord
```

For **Betterdiscord** Users:

CLick > [Download]()


For **Goosemod** Users:

1. Navigate To The Themes Section & Filter By My Name *Leeprky* And Click Install.

# Extras 

Thank you for checking out/downloading my Theme :)
Direct Message Me On @Discord Or Join By **[github.com/leeprky](https://discord.gg/Ff3rqAYB89)** For More Help/Information For Installing/Support. Enjoy :)

## Previews

